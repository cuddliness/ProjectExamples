package com.herexdevelopment.ultimatehub.player;

import com.herexdevelopment.ultimatehub.lootbox.LootBox;
import com.herexdevelopment.ultimatehub.lootbox.LootboxManager;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import com.herexdevelopment.ultimatehub.UltimateHub;

import java.io.File;
import java.io.IOException;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 2-5-18
 */
public class PlayerData {
    public static PlayerData instance;


    public PlayerData(){
        instance = this;
    }


    public FileConfiguration getPlayerConfig(Player p) {
        File file = new File(UltimateHub.getInstance().getDataFolder().getAbsolutePath() + "/players/", p.getUniqueId().toString() + ".yml");
        if(!file.exists())
            addPlayer(p);
        YamlConfiguration conf = new YamlConfiguration();
        try {
            conf.load(file);
            return conf;
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
        return conf;
    }



    public void balancePlayer(Player p){
        File file = new File(UltimateHub.getInstance().getDataFolder().getAbsolutePath() + "/players/", p.getUniqueId().toString() + ".yml");
        for(LootBox lootbox : LootboxManager.getInstance().getLootBoxes()){
            if(getPlayerConfig(p).get("crates." + lootbox.getCrateName()) == null){
                getPlayerConfig(p).set("crates." + lootbox.getCrateName(), 0);

            }
        }

        try {
            getPlayerConfig(p).save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void addPlayer(Player p){
        File file = new File(UltimateHub.getInstance().getDataFolder().getAbsolutePath() + "/players/", p.getUniqueId().toString() + ".yml");
        if(!file.exists()){
            try {
                file.createNewFile();
                getPlayerConfig(p).set("player.name", p.getName());
                getPlayerConfig(p).set("player.uuid", p.getUniqueId().toString());
                getPlayerConfig(p).set("player.coins", 0);
                for(LootBox b : LootboxManager.getInstance().getLootBoxes()){
                    getPlayerConfig(p).set("crates." + b.getCrateName(), 0);
                }

                getPlayerConfig(p).save(file);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static PlayerData getInstance() {
        return instance;
    }
}
