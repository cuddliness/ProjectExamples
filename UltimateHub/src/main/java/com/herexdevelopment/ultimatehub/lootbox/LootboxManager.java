package com.herexdevelopment.ultimatehub.lootbox;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.EnderChest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.herexdevelopment.ultimatehub.UltimateHub;
import com.herexdevelopment.ultimatehub.configmanager.ConfigManager;
import com.herexdevelopment.ultimatehub.utils.ItemBuilder;
import com.herexdevelopment.ultimatehub.utils.StringUtils;
import com.herexdevelopment.ultimatehub.utils.UltimateHologram;

import java.io.*;
import java.util.*;
import java.util.List;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 30-4-18
 */
public class LootboxManager {

    private List<LootBox> lootBoxes = new ArrayList<>();
    private static LootboxManager instance = null;
    private HashMap<String, File> configs = new HashMap<>();

    public LootboxManager() throws IllegalStateException {
        if (instance != null)
            throw new IllegalStateException("Only one instance can run");
        instance = this;
    }

    public static LootboxManager getInstance() {
        if (instance == null)
            throw new IllegalStateException("Cannot get instance: instance is null");
        return instance;
    }

    public void placeLootbox(Player p, String name) {
        LootBox box = getLootbox(name);
        FileConfiguration config = ConfigManager.getInstance().getFileConfiguration("data");

        Block block = p.getLocation().add(0, 1, 0).getBlock();
        if (Material.getMaterial(box.getCrateBlockId()).equals(Material.ENDER_CHEST)) {
            block.setTypeId(box.getCrateBlockId());
            EnderChest c = (EnderChest) block.getState().getData();
            c.setFacingDirection(getPlayerDirection(p.getPlayer()));

        } else {
            block.setTypeId(box.getCrateBlockId());
        }

        String uuid = UUID.randomUUID().toString();
        config.set("crates." + box.getCrateName() + "_" + uuid + ".world", p.getWorld().getName());
        config.set("crates." + box.getCrateName() + "_" + uuid + ".locx", block.getLocation().getBlockX());
        config.set("crates." + box.getCrateName() + "_" + uuid + ".locy", block.getLocation().getBlockY());
        config.set("crates." + box.getCrateName() + "_" + uuid + ".locz", block.getLocation().getBlockZ());
        ConfigManager.getInstance().saveAll();
        Block block1 = p.getLocation().add(0, -1, 0).getBlock();
        block1.setType(Material.ENDER_PORTAL);
        UltimateHologram hologram = new UltimateHologram(p.getLocation().getBlock().getLocation(), box.getHologramlines().get(0));
        hologram.create();
    }


    public LootBox getLootbox(String name) {
        for (LootBox box : lootBoxes) {
            if (box.getCrateName().equalsIgnoreCase(name)) {
                return box;
            }
        }

        return null;
    }


    public void load_lootbox() throws IOException, ParseException {
        loadJsonFiles();
        for (Map.Entry<String, File> ents : configs.entrySet()) {
            JSONParser parser = new JSONParser();
            String json = parser.parse(new FileReader(ents.getValue())).toString();
            JSONObject obs = new JSONObject(json);

            JSONObject settings = obs.getJSONObject("settings");
            JSONObject hologram = obs.getJSONObject("hologram");
            JSONObject effect_event = obs.getJSONObject("effect");
            JSONObject effect_idle = obs.getJSONObject("idle_effect");
            JSONObject items = obs.getJSONObject("items");
            JSONObject crate_interface = obs.getJSONObject("crate_interface");
            List<String> hologram_lines = new ArrayList<>();
            JSONArray lines = hologram.getJSONArray("lines");
            for (int i = 0; i < lines.length(); ++i) {
                hologram_lines.add(StringUtils.repColor(lines.get(i).toString()));
            }

            List<String> event_list = new ArrayList<>();
            JSONArray evs = effect_event.getJSONArray("effect_event");
            for (int i = 0; i < evs.length(); ++i) {
                event_list.add(StringUtils.repColor(evs.get(i).toString()));
            }

            List<String> event_list_idle = new ArrayList<>();
            JSONArray evs_idle = effect_idle.getJSONArray("effect_event");
            for (int i = 0; i < evs_idle.length(); ++i) {
                event_list_idle.add(StringUtils.repColor(evs_idle.get(i).toString()));
            }

            LootBox box = new LootBox(crate_interface.getInt("inv_size"), settings.getString("crate_name"), settings.getInt("crate_block_id"), hologram_lines, event_list, event_list_idle, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
            Set<String> keys = obs.getJSONObject("items").keySet();
            for (String k : keys) {
                JSONObject lootitem = obs.getJSONObject("items").getJSONObject(k);
                LootItem item = new LootItem(lootitem.getInt("sound_id"), lootitem.getInt("item_id"),
                        lootitem.getString("displayname"), lootitem.getString("permission"),
                        lootitem.getString("command"), lootitem.getString("message"),
                        lootitem.getBoolean("withpermission"), lootitem.getBoolean("withcommand"));
                box.getLootitems().add(item);

            }

            Set<String> keys1 = obs.getJSONObject("crate_interface").getJSONObject("items").keySet();
            for (String k : keys1) {
                JSONObject item = obs.getJSONObject("crate_interface").getJSONObject("items").getJSONObject(k);
                ItemStack i = new ItemBuilder(Material.getMaterial(item.getInt("id"))).name(StringUtils.repColor(item.getString("displayname"))).amount(1).build();
                box.getInterfaceItems().add(new InterfaceItem(item.getInt("id"), item.getInt("slotID"), item.getString("displayname"), item.getString("event")));
            }


            for (String key : ConfigManager.getInstance().getConfig("data").getConfiguration().getConfigurationSection("crates").getKeys(false)) {
                if (key.contains(box.getCrateName())) {
                    World w = Bukkit.getServer().getWorld(ConfigManager.getInstance().getConfig("data").getConfiguration().getString("crates." + key + ".world"));
                    double x = ConfigManager.getInstance().getConfig("data").getConfiguration().getDouble("crates." + key + ".locx");
                    double y = ConfigManager.getInstance().getConfig("data").getConfiguration().getDouble("crates." + key + ".locy");
                    double z = ConfigManager.getInstance().getConfig("data").getConfiguration().getDouble("crates." + key + ".locz");

                    box.getLocs().add(new Location(w, x, y, z));
                }
            }

            lootBoxes.add(box);


        }

    }


    private void loadJsonFiles() {
        File dir = new File(UltimateHub.getInstance().getDataFolder().getAbsolutePath() + "/crates/");
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".json");
            }
        });
        for (int i = 0; i < files.length; i++) {
            if (files[i].getName().contains("crate")) {
                configs.put(files[i].getName(), files[i]);
            }
        }


    }

    public BlockFace getPlayerDirection(Player player)
    {

        BlockFace dir = null;

        float y = player.getLocation().getYaw();

        if( y < 0 ){y += 360;}

        y %= 360;

        int i = (int)((y+8) / 22.5);

        if(i == 0){dir = BlockFace.WEST;}
        else if(i == 1){dir = BlockFace.WEST_NORTH_WEST;}
        else if(i == 2){dir = BlockFace.NORTH_WEST;}
        else if(i == 3){dir = BlockFace.NORTH_NORTH_WEST;}
        else if(i == 4){dir = BlockFace.NORTH;}
        else if(i == 5){dir = BlockFace.NORTH_NORTH_EAST;}
        else if(i == 6){dir = BlockFace.NORTH_EAST;}
        else if(i == 7){dir = BlockFace.EAST_NORTH_EAST;}
        else if(i == 8){dir = BlockFace.EAST;}
        else if(i == 9){dir = BlockFace.EAST_SOUTH_EAST;}
        else if(i == 10){dir = BlockFace.SOUTH_EAST;}
        else if(i == 11){dir = BlockFace.SOUTH_SOUTH_EAST;}
        else if(i == 12){dir = BlockFace.SOUTH;}
        else if(i == 13){dir = BlockFace.SOUTH_SOUTH_WEST;}
        else if(i == 14){dir = BlockFace.SOUTH_WEST;}
        else if(i == 15){dir = BlockFace.WEST_SOUTH_WEST;}
        else {dir = BlockFace.WEST;}

        return dir;

    }

    public List<LootBox> getLootBoxes() {
        return lootBoxes;
    }

}
