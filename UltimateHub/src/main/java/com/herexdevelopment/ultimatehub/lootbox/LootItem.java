package com.herexdevelopment.ultimatehub.lootbox;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 30-4-18
 */
public class LootItem {

   private int soundId, itemId;
   private String displayname, permission, command, message;
   private boolean withpermission, withcommand;


    public LootItem(int soundId, int itemId, String displayname, String permission, String command, String message, boolean withpermission, boolean withcommand) {
        this.soundId = soundId;
        this.itemId = itemId;
        this.displayname = displayname;
        this.permission = permission;
        this.command = command;
        this.message = message;
        this.withpermission = withpermission;
        this.withcommand = withcommand;
    }
    public int getSoundId() {
        return soundId;
    }

    public int getItemId() {
        return itemId;
    }

    public String getDisplayname() {
        return displayname;
    }

    public String getPermission() {
        return permission;
    }

    public String getCommand() {
        return command;
    }

    public String getMessage() {
        return message;
    }

    public boolean isWithpermission() {
        return withpermission;
    }

    public boolean isWithcommand() {
        return withcommand;
    }


}
