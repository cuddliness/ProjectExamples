package com.herexdevelopment.ultimatehub.configmanager.json;

import com.google.gson.*;
import com.herexdevelopment.ultimatehub.UltimateHub;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class JsonConfig {

    String filename;
    File file;
    File folder;


    public JsonConfig(String name, String json) {
        try {
            new JsonConfig(name, UltimateHub.getInstance().getDataFolder(), json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JsonConfig(final String name, final File folder, String json) throws IOException {
        if (UltimateHub.getInstance() == null) {
            throw new IllegalArgumentException("plugin cannot be null!");
        }
        if (folder == null) {
            throw new IllegalStateException();
        }

        this.filename = name + ".json";
        this.folder = folder;
        this.file = new File(folder , name + ".json");
        if (!file.exists()) {
            folder.mkdirs();
            file.createNewFile();
            register(json);
        }
    }


    public org.json.JSONObject getJsonObject(String name){
        try {
            org.json.JSONObject json = new org.json.JSONObject(readFile());
            org.json.JSONObject toget = json.getJSONObject(name);
            return toget;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }


    public ArrayList<String> getKeysFromJson(String name)
    {
        org.json.JSONObject mainObject = new org.json.JSONObject(readFile());

        org.json.JSONObject posts = mainObject.getJSONObject(name);

        ArrayList<String> map = new ArrayList<>();
        map.addAll(posts.keySet());
        return map;
    }




    public void register(String jsonline) {
        try {
            BufferedReader r = new BufferedReader(new FileReader(getFile()));
            try (FileWriter writer = new FileWriter(getFile())) {
                Gson g = new GsonBuilder().setPrettyPrinting().create();
                JsonParser jp = new JsonParser();
                JsonElement je = jp.parse(jsonline);
                String prettyJsonString = g.toJson(je);
                writer.write(prettyJsonString);
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            r.close();

        } catch (IOException e) {
            System.out.print("Cannot register file, IO Exeption");
        }

    }


    public String getRaw() {
        Gson g = new GsonBuilder().setPrettyPrinting().create();
        String prettyJsonString = g.toJson(readFile());
        return prettyJsonString;
    }


    public String readFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(getFile()))) {
            String line;
            StringBuilder r = new StringBuilder();
            while ((line = br.readLine()) != null) {
                r.append(line);
            }
            return r.toString();
        } catch (Exception e) {
            System.out.print("Unable to read file '" + filename + "'");
            System.out.print(e.toString());
        }
        return "";
    }



    public HashMap<String, JsonElement> getElement() {
        HashMap<String, JsonElement> entries = (HashMap<String, JsonElement>) getObjects();//will return members of your object
        String yourJson = null;
        JsonParser parser = new JsonParser();
        JsonElement element = null;
        element = parser.parse(getRaw());
        JsonObject obj = element.getAsJsonObject(); //since you know it's a JsonObject
        return entries;
    }


    public Set getObjects() {
        String yourJson = null;
        yourJson = getRaw();
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(yourJson);
        JsonObject obj = element.getAsJsonObject(); //since you know it's a JsonObject
        return obj.entrySet();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getFolder() {
        return folder;
    }

    public void setFolder(File folder) {
        this.folder = folder;
    }

    public File getFile(){
        return this.file;
    }





}
