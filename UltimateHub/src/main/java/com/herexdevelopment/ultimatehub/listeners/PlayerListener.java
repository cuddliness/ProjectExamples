package com.herexdevelopment.ultimatehub.listeners;

import com.herexdevelopment.ultimatehub.scoreboard.ScoreBoard;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import com.herexdevelopment.ultimatehub.joindata.JoinItem;
import com.herexdevelopment.ultimatehub.menu.MenuDataManager;
import com.herexdevelopment.ultimatehub.player.PlayerData;
import com.herexdevelopment.ultimatehub.utils.ItemBuilder;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 29-4-18
 */
public class PlayerListener implements Listener {


    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        e.getPlayer().getInventory().clear();
        if(!e.getPlayer().hasPlayedBefore())
            PlayerData.getInstance().addPlayer(e.getPlayer());
            PlayerData.getInstance().balancePlayer(e.getPlayer());
        for(JoinItem item : MenuDataManager.getInstance().getJoinitems()){
            ItemStack i  = new ItemBuilder(Material.getMaterial(item.getItemid())).name(item.getDisplayname()).amount(1).lore(item.getLore()).build();
            e.getPlayer().getInventory().setItem(item.getSlot_id(), i);
        }

        ScoreBoard.getInstance().setBoard(e.getPlayer());
        PlayerData.getInstance().balancePlayer(e.getPlayer());

    }
}
