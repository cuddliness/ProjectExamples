package com.herexdevelopment.ultimatehub.particle;

import com.herexdevelopment.ultimatehub.menu.MenuDataManager;
import org.bukkit.configuration.file.FileConfiguration;
import com.herexdevelopment.ultimatehub.configmanager.ConfigManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 2-5-18
 */
public class ParticleManager {
    List<ParticleDesign> particleDesigns = new ArrayList<>();
    private static ParticleManager instance = null;

    public ParticleManager() throws IllegalStateException {
        if (instance != null)
            throw new IllegalStateException("Only one instance can run");
        instance = this;
    }

    public static ParticleManager getInstance() {
        if (instance == null)
            throw new IllegalStateException("Cannot get instance: instance is null");
        return instance;
    }

    public void loadALL(){
        for (String key : ConfigManager.getInstance().getConfig("particles").getConfiguration().getConfigurationSection("particles").getKeys(false)) {
            FileConfiguration config = ConfigManager.getInstance().getConfig("particles").getConfiguration();
            particleDesigns.add(new ParticleDesign(key, config.getInt("particles." + key + ".offsetX"),config.getInt("particles." + key + ".offsetY"), config.getInt("particles." + key + ".offsetZ"), config.getInt("particles." + key + ".speed"), config.getInt("particles." + key + ".amount")));

        }


        }

    public List<ParticleDesign> getParticleDesigns() {
        return particleDesigns;
    }


    public ParticleDesign getParticle(String name){
        for(ParticleDesign des : particleDesigns){
            if(des.getName().equalsIgnoreCase(name)){
                return des;
            }
        }

        return null;

    }
}
