package com.herexdevelopment.ultimatehub.message;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import com.herexdevelopment.ultimatehub.configmanager.ConfigManager;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 30-4-18
 */
public enum Message {

    PREFIX("PREFIX"),

    NO_PERMMISION("NO_PERMISSION"),

    TO_MANY_ARGUMENTS("TO_MANY_ARGUMENTS"),

    NO_SPACES_PERMISSON("NO_SPACES_PERMISSON"),

    PLAYER_QUIT();

    /**
     * Location of the message
     */
    private String messageLocation;

    /**
     * Constructor the message will be added to the "message." location by default
     */
    Message() {
        this.messageLocation = "message";
    }

    /**
     * Constructor the message will be added to the "message." location by default
     *
     * @param messageLocation the parent location of the message
     */
    Message(String messageLocation) {
        this.messageLocation = messageLocation;
    }

    /**
     * Get the string from the messages.yml related to the selected message
     * @return the string from the config (color codes will be applied automatically '&' char )
     */
    public String getString() {
        FileConfiguration messages = ConfigManager.getInstance().getFileConfiguration("messages");
        String location = messageLocation + "." + toString().toLowerCase();
        if (messages.getString(location) == null)
            return ChatColor.translateAlternateColorCodes('&', "&cNULL: " + location);
        return ChatColor.translateAlternateColorCodes('&', messages.getString(location));
    }



    /**
     * Send the current message to a command sender
     * @param sender the sender you want to send it to
     */
    public void send(CommandSender sender) {
        sender.sendMessage(this.getString());
    }


    public void send(CommandSender sender, Replaceable replaceable) {
        final String[] msg = new String[]{getString()};
        replaceable.forEach((key, value) ->
                msg[0] = msg[0].replace(key, value.toString()));
        msg[0] = ChatColor.translateAlternateColorCodes('&', msg[0]);
        sender.sendMessage(msg[0]);
    }
}
