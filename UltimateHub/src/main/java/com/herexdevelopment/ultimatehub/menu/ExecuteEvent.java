package com.herexdevelopment.ultimatehub.menu;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.herexdevelopment.ultimatehub.lootbox.LootBox;
import com.herexdevelopment.ultimatehub.utils.StringUtils;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import com.herexdevelopment.ultimatehub.UltimateHub;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 29-4-18
 */
public class ExecuteEvent {

    private static ExecuteEvent instance = null;
    public ExecuteEvent() throws IllegalStateException {
        if (instance != null)
            throw new IllegalStateException("Only one instance can run");
        instance = this;
    }

    public static ExecuteEvent getInstance() {
        if (instance == null)
            throw new IllegalStateException("Cannot get instance: instance is null");
        return instance;
    }

    public void execute(LootBox b, Player p, String event){
        String stripped = event.replace("}", "").replace("{", "");
        String[] args = stripped.split(":");
        switch (args[0]) {
            case "servertp":
                servertp(p, args[1]);
                break;
            case "msg":
                p.sendMessage(StringUtils.repColor(args[1]));
                p.closeInventory();
            case "open":
                if(args[1].equalsIgnoreCase("crate")){
                    for(Location loc : b.getLocs()){
                        if(loc.distanceSquared(p.getLocation()) <= 4 * 4){
                            p.closeInventory();
                            b.startPlayEffect(loc);
                        }
                    }
                }

        }
    }


    public void execute(Player p, String event) {
        String stripped = event.replace("}", "").replace("{", "");
        String[] args = stripped.split(":");
        switch (args[0]) {
            case "servertp":
                servertp(p, args[1]);
                break;
            case "msg":
                p.sendMessage(StringUtils.repColor(args[1]));
                p.closeInventory();
                break;
        }
    }


    private void servertp(Player p, String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        p.sendPluginMessage(UltimateHub.getInstance(), "BungeeCord", out.toByteArray());
        p.closeInventory();

    }
}
