package com.mortalpvp.mpkingdomdungeon.manager;

import com.mortalpvp.core.api.Module;
import com.mortalpvp.core.api.MortalAPI;
import com.mortalpvp.core.bukkit.util.particles.ParticleEffect;
import com.mortalpvp.core.common.chat.C;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.manager.dungeonmanager.Dungeon;
import com.mortalpvp.mpkingdomdungeon.manager.editmanager.EditPlayer;
import com.mortalpvp.mpkingdomdungeon.manager.filemanager.Config;
import com.mortalpvp.mpkingdomdungeon.ui.ConfirmGUI;
import com.mortalpvp.mpkingdomdungeon.utils.ActionBar;
import com.mortalpvp.mpkingdomdungeon.utils.TitleManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.io.File;
import java.util.*;

public class DungeonManager extends Module {
    private final LinkedList<Dungeon> dungeons = new LinkedList<>();
    private final HashMap<UUID, Dungeon> playersInRegion = new HashMap<>();
    private final LinkedList<Config> configs = new LinkedList<>();
    private final List<EditPlayer> inEditing = new ArrayList<>();

    public DungeonManager(MortalAPI api, String name) {
        super(api, name);
        loadALL();
        checkAreas();
    }

    private void loadALL(){
        dungeons.clear();
        File folder = new File(Main.getInstance().getDataFolder().getAbsolutePath() + "/dungeons/");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                configs.add(new Config(listOfFiles[i].getName().replace(".yml", ""), new File(Main.getInstance().getDataFolder().getAbsolutePath() + "/dungeons/")));
            }
        }
        for(Config cf : configs){
            List<Location> portlocs = new ArrayList<>();
            List<ItemStack> rewarditems = new ArrayList<>();
            HashMap<Location, Integer> mobspawns = new HashMap<>();
            HashMap<Location, Integer> stages = new HashMap<>();
            portlocs.add(new Location(Bukkit.getServer().getWorld(cf.getConfig().getString("settings.world")), cf.getConfig().getDouble("selection.port.posx"), cf.getConfig().getDouble("selection.port.posy"), cf.getConfig().getDouble("selection.port.posz")));
            portlocs.add(new Location(Bukkit.getServer().getWorld(cf.getConfig().getString("settings.world")), cf.getConfig().getDouble("selection.port.pos2x"), cf.getConfig().getDouble("selection.port.pos2y"), cf.getConfig().getDouble("selection.port.pos2z")));
            if(!(cf.getConfig().getConfigurationSection("rewarditems") == null)) {
                for (String key : cf.getConfig().getConfigurationSection("rewarditems").getKeys(false)) {
                    rewarditems.add(cf.getConfig().getItemStack("rewarditems." + key + ".stack"));
                }
            }

            if(!(cf.getConfig().getConfigurationSection("spawns.mob") == null)) {
                for (String key : cf.getConfig().getConfigurationSection("spawns.mob").getKeys(false)) {
                    Location loc = new Location(Bukkit.getWorld(cf.getConfig().getString("settings.world")), cf.getConfig().getDouble("spawns.mob." + key + ".x"), cf.getConfig().getDouble("spawns.mob." + key + ".y"), cf.getConfig().getDouble("spawns.mob." + key + ".z"));
                    mobspawns.put(loc, cf.getConfig().getInt("spawns.mob." + key + ".id"));
                }

            }

            if(!(cf.getConfig().getConfigurationSection("stages") == null)) {
                for (String key : cf.getConfig().getConfigurationSection("stages").getKeys(false)) {
                    Location loc = new Location(Bukkit.getWorld(cf.getConfig().getString("settings.world")), cf.getConfig().getDouble("stages." + key + ".x"), cf.getConfig().getDouble("stages." + key + ".y"), cf.getConfig().getDouble("stages." + key + ".z"));
                    stages.put(loc, cf.getConfig().getInt("stages." + key + ".id"));
                }
            }
            Dungeon d = new Dungeon(cf.getConfig().getString("settings.name"), cf.getConfig().getString("settings.world"), cf.getConfig().getInt("settings.bossid"), new Location(Bukkit.getServer().getWorld(cf.getConfig().getString("settings.world")), cf.getConfig().getDouble("spawns.mobs.boss.x"), cf.getConfig().getDouble("spawns.mobs.boss.y"), cf.getConfig().getDouble("spawns.mobs.boss.z")),
                    portlocs , rewarditems, stages,mobspawns , cf.getConfig().getInt("settings.level"), new Location[]{new Location(Bukkit.getServer().getWorld(cf.getConfig().getString("settings.world")), cf.getConfig().getDouble("selection.arena.posx"), cf.getConfig().getDouble("selection.arena.posy"), cf.getConfig().getDouble("selection.arena.posz")), new Location(Bukkit.getServer().getWorld(cf.getConfig().getString("settings.world")), cf.getConfig().getDouble("selection.arena.pos2x"), cf.getConfig().getDouble("selection.arena.pos2y"), cf.getConfig().getDouble("selection.arena.pos2z"))});

            dungeons.add(d);
        }
    }

    public String getDunLore(Dungeon dungeon){
        StringBuilder added = new StringBuilder();
        StringBuilder missed = new StringBuilder();
        added.append(ChatColor.translateAlternateColorCodes('&', "&a&l" + "Added values:\n\n"));
        missed.append(ChatColor.translateAlternateColorCodes('&', "&4&l" + "Missed values:\n\n"));

        if(dungeon.getRewarditems().isEmpty()){
            missed.append(ChatColor.translateAlternateColorCodes('&', "&4- " + "Reward items\n"));
        }else{
            added.append(ChatColor.translateAlternateColorCodes('&', "&a- " + "Reward items\n"));
        }
        if(dungeon.getMobspawnpoints().isEmpty()){
            missed.append(ChatColor.translateAlternateColorCodes('&', "&4- " + "Mob spawnpoints\n"));
        }else{
            added.append(ChatColor.translateAlternateColorCodes('&', "&a- " + "Mob spawnpoints\n"));
        }
        if(dungeon.getPortlocations().isEmpty() && dungeon.getPortlocations().get(0).getZ() != 0 && dungeon.getPortlocations().get(0).getY() != 0 && dungeon.getPortlocations().get(0).getX() != 0){
            missed.append(ChatColor.translateAlternateColorCodes('&', "&4- " + "Port locations\n"));
        }else{
            added.append(ChatColor.translateAlternateColorCodes('&', "&a- " + "Port locations\n"));
        }

        if(dungeon.getBosid() == 0){
            missed.append(ChatColor.translateAlternateColorCodes('&', "&4- " + "Bos-ID\n"));
        }else{
            added.append(ChatColor.translateAlternateColorCodes('&', "&a- " + "Bos-ID\n"));
        }

        if(dungeon.getStages().isEmpty()){
            missed.append(ChatColor.translateAlternateColorCodes('&', "&4- " + "Stages locations\n"));
        }else{
            added.append(ChatColor.translateAlternateColorCodes('&', "&a- " + "Stages locations\n"));
        }
        if(dungeon.getBosslocation().getX() != 0 && dungeon.getBosslocation().getY() != 0 && dungeon.getBosslocation().getZ() != 0){
            missed.append(ChatColor.translateAlternateColorCodes('&', "&4- " + "Boss location\n"));
        }else{
            added.append(ChatColor.translateAlternateColorCodes('&', "&a- " + "Boss location\n"));
        }

        if(dungeon.getName() == null){
            missed.append(ChatColor.translateAlternateColorCodes('&', "&4- " + "Dungeon name\n"));
        }else{
            added.append(ChatColor.translateAlternateColorCodes('&', "&a- " + "Dungeon name\n"));
        }
        if(dungeon.getLevel() == 0){
            missed.append(ChatColor.translateAlternateColorCodes('&', "&4- " + "Dungeon Level\n"));
        }else{
            added.append(ChatColor.translateAlternateColorCodes('&', "&a- " + "Dungeon Level\n"));
        }
        if(dungeon.getWorld() == null){
            missed.append(ChatColor.translateAlternateColorCodes('&', "&4- " + "Dungeon world\n"));
        }else{
            added.append(ChatColor.translateAlternateColorCodes('&', "&a- " + "Dungeon world\n"));
        }

        return added.append("\n\n").toString() + missed.append("\n(Right-Click) &c= Go to the dungeon\n(Left-Click) &c= Edit the dungeon").toString();
    }

    private void checkAreas(){
        ActionBar bar = new ActionBar();
        bar.setTxt(ChatColor.translateAlternateColorCodes('&', "&5You entered: &f<name>" ));
        BukkitTask task = Bukkit.getScheduler().runTaskTimerAsynchronously(Main.getPlugin(Main.class), new Runnable() {
            @Override
            public void run() {
                if(Bukkit.getOnlinePlayers().isEmpty()){
                    return;
                }
                for(Dungeon dungeon : dungeons) {
                    if(!dungeon.getPortlocations().isEmpty()){
                        ParticleEffect.SMOKE_LARGE.display(0, 5, 0, 0, 100, dungeon.getPortlocations().get(0), 100);
                        ParticleEffect.SMOKE_LARGE.display(0, -10, 0, 0, 100, dungeon.getPortlocations().get(1), 100);
                        ParticleEffect.SPELL_WITCH.display(0, 5, 0, 0, 100, dungeon.getPortlocations().get(0), 100);
                        ParticleEffect.SPELL_WITCH.display(0, -10, 0, 0, 100, dungeon.getPortlocations().get(1), 100);
                    }
                    for(Player p : Bukkit.getOnlinePlayers()){

                        if(dungeon.isInRect(p, dungeon.getArenalocs()[0], dungeon.getArenalocs()[1])){
                            bar.setTxt(bar.getTxt().replace("<name>", dungeon.getName()));
                            bar.sendToPlayer(p);
                            if(!playersInRegion.containsKey(p.getUniqueId())){
                                TitleManager.sendTitle(p, ChatColor.translateAlternateColorCodes('&', "&5You entered"), ChatColor.translateAlternateColorCodes('&', "&f" + dungeon.getName()),5 ,5,5);
                                playersInRegion.put(p.getUniqueId(), dungeon);
                            }
                        }else if (playersInRegion.containsKey(p.getUniqueId())){
                            TitleManager.sendTitle(p, ChatColor.translateAlternateColorCodes('&', "&5You Leaved"), ChatColor.translateAlternateColorCodes('&', "&f" + dungeon.getName()),5 ,5,5);
                            playersInRegion.remove(p.getUniqueId(), dungeon);
                        }

                        if(dungeon.isInRect(p, dungeon.getPortlocations().get(0), dungeon.getPortlocations().get(1))){
                            pushAwayEntity(p, 30);
                            p.sendMessage(C.TAC("&7[&4HEY!&7] &5You sure you wanna enter the dungeon: &f" + dungeon.getName()));
                            new ConfirmGUI(Main.getInstance(), C.TAC("&7[&4HEY!&7] &5You sure you wanna enter the dungeon: &f" + dungeon.getName())).open(p);
                            p.setFallDistance(0);
                        }

                    }
                    }
            }
        },10L,10L);
    }

    public void pushAwayEntity(Player player, double speed) {

        Location playerCenterLocation = player.getEyeLocation();
        Location playerToThrowLocation = player.getEyeLocation();

        double x = playerToThrowLocation.getX() - playerCenterLocation.getX();
        double y = playerToThrowLocation.getY() - playerCenterLocation.getY();
        double z = playerToThrowLocation.getZ() - playerCenterLocation.getZ();

        Vector throwVector = new Vector(x, y, z);

        throwVector.normalize();
        throwVector.multiply(1D);
        throwVector.setY(0.5D);
        throwVector.multiply(1).setX(-2);
        player.setVelocity(throwVector);
    }

    public List<EditPlayer> getInEditing() {
        return inEditing;
    }

    /**
     *
     * @param w World where the dungeon is created
     * @param name name of the dungeon
     **/
    public boolean createDungeon(World w, String name, Location pos1, Location pos2){
        Config cf = new Config(name, new File(Main.getInstance().getDataFolder().getAbsolutePath() + "/dungeons/"));
        cf.getConfig().set("settings.name", name);
        cf.getConfig().set("settings.world", w.getName());
        cf.getConfig().set("settings.level", 0);
        cf.getConfig().set("settings.bossid", 0);

        cf.getConfig().set("selection.arena.posx", pos1.getX());
        cf.getConfig().set("selection.arena.posy", pos1.getY());
        cf.getConfig().set("selection.arena.posz", pos1.getZ());
        cf.getConfig().set("selection.arena.pos2x", pos2.getX());
        cf.getConfig().set("selection.arena.pos2y", pos2.getY());
        cf.getConfig().set("selection.arena.pos2z", pos2.getZ());

        cf.getConfig().addDefault("selection.port.posx", 0);
        cf.getConfig().addDefault("selection.port.posy", 0);
        cf.getConfig().addDefault("selection.port.posz",0 );

        cf.getConfig().addDefault("selection.port.pos2x", 0);
        cf.getConfig().addDefault("selection.port.pos2y", 0);
        cf.getConfig().addDefault("selection.port.pos2z",0 );
        cf.getConfig().addDefault("stages", "");
        cf.getConfig().addDefault("spawns.mobs", "");
        cf.getConfig().addDefault("spawns.mobs.boss.x", 0);
        cf.getConfig().addDefault("spawns.mobs.boss.y", 0);
        cf.getConfig().addDefault("spawns.mobs.boss.z", 0);

        cf.saveConfig();
        loadALL();
        return true;
    }

    public LinkedList<Config> getConfigs() {
        return configs;
    }

    public LinkedList<Dungeon> getDungeons() {
        return dungeons;
    }
}
