package com.mortalpvp.mpkingdomdungeon.manager.editmanager;

import com.mortalpvp.mpkingdomdungeon.manager.dungeonmanager.Dungeon;

import java.util.UUID;

public class EditPlayer {
    private UUID uuid;
    private EditType editType;
    private Dungeon dungeon;

    public EditPlayer(UUID uuid, EditType editType, Dungeon dungeon) {
        this.uuid = uuid;
        this.editType = editType;
        this.dungeon = dungeon;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public EditType getEditType() {
        return editType;
    }

    public void setEditType(EditType editType) {
        this.editType = editType;
    }

    public Dungeon getDungeon() {
        return dungeon;
    }

    public void setDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
    }
}
