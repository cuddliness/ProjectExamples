package com.mortalpvp.mpkingdomdungeon.manager.editmanager;

public enum MobTypeBuilder {
    SKELETON("SKELETON"),    WITHER_SKELETON("WITHER_SKELETON"), ZOMBIE("ZOMBIE"), CREEPER("CREEPER"), ENDERMAN("ENDERMAN"), SILVERFISH("SILVERFISH"), SPIDER("SPIDER"), WOLF("WOLF"),OCELOT("OCELOT"), WITCH("WITCH");
    private final String text;
    MobTypeBuilder(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
