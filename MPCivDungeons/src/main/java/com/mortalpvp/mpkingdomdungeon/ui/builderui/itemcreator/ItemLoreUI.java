package com.mortalpvp.mpkingdomdungeon.ui.builderui.itemcreator;

import com.mortalpvp.core.api.ui.UI;
import com.mortalpvp.core.api.ui.components.UIComponent;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.core.common.chat.C;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.MobBuilder;
import com.mortalpvp.mpkingdomdungeon.ui.builderui.BuilderHome;
import com.mortalpvp.mpkingdomdungeon.ui.builderui.ItemCreatorUI;
import com.mortalpvp.mpkingdomdungeon.ui.builderui.WichMenu;
import com.mortalpvp.mpkingdomdungeon.utils.inputgui.AnvilGUI;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemLoreUI extends UI {

    public ItemLoreUI(Main plugin, MobBuilder mobBuilder, Player pl, WichMenu menu) {

        super(plugin, "ItemCreator", 9 * 6, InventoryType.CHEST);

        UIComponent border = new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                ItemStack i = new ItemCreator(Material.STAINED_GLASS_PANE).setDurability((short) 10).Create();
                return i;
            }
        };

        UIComponent currentitem = new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return plugin.getItemCreatorManager().getStackFromQueue(player);

            }
        };
        startRefreshTask(10);
        setComponent(0, border);
        setComponent(1, border);
        setComponent(2, border);
        setComponent(3, border);
        setComponent(4, border);
        setComponent(5, border);
        setComponent(6, border);
        setComponent(7, border);
        setComponent(8, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.EMERALD_BLOCK).setDisplayName("&aAdd line").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                new AnvilGUI(plugin, player, "Type a line", ((p, output) -> {
                    player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Added line: " + output));
                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 1, 1);
                    ItemStack newitem = new ItemStack(currentitem.getItemStack(player).getType());

                    ItemMeta meta = currentitem.getItemStack(player).getItemMeta();
                    java.util.List<String> lores;
                    if (!meta.hasLore()) {
                        lores = new ArrayList<>();
                    } else {
                        lores = meta.getLore();
                    }
                    lores.add(C.TAC(output));
                    meta.setLore(lores);
                    meta.getLore().add(output);
                    newitem.setItemMeta(meta);
                    plugin.getItemCreatorManager().ApplayChanges(player, currentitem.getItemStack(player), newitem);
                    new ItemCreatorUI(mobBuilder, newitem, menu, plugin).open(player);
                    return null;
                }), mobBuilder);
            }
        }));

        setComponent(9, border);
        setComponent(17, border);
        setComponent(18, border);
        setComponent(26, border);
        setComponent(27, border);
        setComponent(35, border);
        setComponent(36, border);
        setComponent(44, border);
        setComponent(45, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.ARROW).setDisplayName("&6Back").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                new BuilderHome(mobBuilder, plugin).open(player);
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 1, 1);
            }
        }));
        setComponent(46, border);
        setComponent(47, border);
        setComponent(48, border);
        setComponent(49, border);
        setComponent(50, border);
        setComponent(51, border);
        setComponent(52, border);
        setComponent(53, border);
        ItemMeta meta = currentitem.getItemStack(pl).getItemMeta();
        if (meta.getLore() == null || meta == null) {
            return;
        }
        meta.getLore().forEach(item -> addComponent(new UIComponent() {

            @Override
            public ItemStack getItemStack(Player player) {
                ItemCreator creator = new ItemCreator(Material.PAPER).setDisplayName(C.TAC(item)).addLore(C.TAC("&7(RightClick) &cTo remove this line!"));
                return creator.Create();
            }
        }.onInteract((player, clickType) -> {
            List<String> lores = currentitem.getItemStack(player).getItemMeta().getLore();
            lores.stream().forEach(s -> {
                if(s.contains(currentitem.getItemStack(player).getItemMeta().getDisplayName())){
                    lores.remove(s);
                    currentitem.getItemStack(player).getItemMeta().setLore(lores);
                }
            });
        })));

    }
}
