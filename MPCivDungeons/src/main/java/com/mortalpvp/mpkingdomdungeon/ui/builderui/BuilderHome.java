package com.mortalpvp.mpkingdomdungeon.ui.builderui;

import com.mortalpvp.core.api.ui.UI;
import com.mortalpvp.core.api.ui.components.UIComponent;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.core.common.chat.C;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.MobBuilder;
import com.mortalpvp.mpkingdomdungeon.ui.builderui.itemcreator.LootItemsUI;
import com.mortalpvp.mpkingdomdungeon.utils.inputgui.AnvilGUI;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

public class BuilderHome extends UI {
    public BuilderHome(MobBuilder mobBuilder, Main plugin) {
        super(plugin, "MobBuilder", 9 * 6, InventoryType.CHEST);
        UIComponent border = new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                ItemStack i = new ItemCreator(Material.STAINED_GLASS_PANE).setDurability((short) 10).Create();
                return i;
            }
        };

        setComponent(20, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&6&lChange the mob-type")
                        .addLores("&7Change the mobtype to a other mob!").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                new EditMobType(plugin, mobBuilder).open(player);
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HAT, 1, 1);

            }
        }));
        setComponent(22, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.CHEST).setDisplayName("&6&lChange the mob-loot items")
                        .addLores("&7Add or remove mob-loot items").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                new LootItemsUI(plugin, mobBuilder).open(player);
            }
        }));
        setComponent(24, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.BLAZE_ROD).setDisplayName("&6&lChange the spells")
                        .addLores("&7Add remove or create a spell for your mob").Create();
            }
        });

        setComponent(30, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.REDSTONE).setDisplayName("&6&lChange the Default settings")
                        .addLores(new String[] { "&7Change default settings like:", "&6- Follow range", "&6- Speed",
                                "&6- Potion effects", "&7And much more!" })
                        .Create();
            }
        });

        setComponent(32, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.DIAMOND_SWORD).setDisplayName("&6&lChange Mob's Inventory")
                        .addLores(new String[] { "&7Add items like armour our weapon's" }).Create();
            }
        });
        setComponent(40, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.POTION).setDisplayName("&6&lChange Mob's Weaknesses")
                        .setPotionBasePotionData(new PotionData(PotionType.WEAKNESS))
                        .addItemFlags(ItemFlag.HIDE_ATTRIBUTES).addItemFlags(ItemFlag.HIDE_POTION_EFFECTS)
                        .addLores(new String[] { "&7Add,Remove or create weaknesses for your mob" }).Create();
            }
        });
        setComponent(1, border);
        setComponent(2, border);
        setComponent(3, border);
        setComponent(4, border);
        setComponent(5, border);
        setComponent(6, border);
        setComponent(7, border);
        setComponent(9, border);
        setComponent(17, border);
        setComponent(18, border);
        setComponent(26, border);
        setComponent(27, border);
        setComponent(35, border);
        setComponent(36, border);
        setComponent(44, border);
        setComponent(45, border);
        setComponent(46, border);
        setComponent(47, border);
        setComponent(48, border);
        setComponent(50, border);
        setComponent(51, border);
        setComponent(52, border);
        setComponent(53, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.BREWING_STAND_ITEM).setDisplayName("&6Overview")
                        .addLores(new String[] { "&7Check all your modifed changes to your mob-builder" }).Create();
            }
        }.onInteract((player, clickType) -> {
            new OverviewUI(plugin, mobBuilder).open(player);
        }));

        setComponent(49, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.EMERALD).setDisplayName("&a&lFinish")
                        .addLores(new String[] { "&7Finish the building of your CUSTOM-MOB!" }).Create();
            }
        });
        setComponent(8, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.EXP_BOTTLE).setDisplayName("&6&lChange Mob-Level")
                        .addLores(new String[] { "&7Edit mob level, based on skills" }).Create();
            }
        });
        setComponent(0, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.PAPER).setDisplayName("&6&lChange Name")
                        .addLores(new String[] { "&7Left click to change the name of your custom mob" }).Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                new AnvilGUI(plugin, player, "Type a name", ((p, output) -> {
                    mobBuilder.setName(output);
                    player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed name to: " + output));
                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 1, 1);
                    new BuilderHome(mobBuilder, plugin).open(p);
                    return null;
                }), mobBuilder);
            }
        }));

    }
}
