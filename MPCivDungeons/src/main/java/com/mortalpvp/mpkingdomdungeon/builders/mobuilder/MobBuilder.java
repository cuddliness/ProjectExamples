package com.mortalpvp.mpkingdomdungeon.builders.mobuilder;

import com.mortalpvp.mpkingdomdungeon.builders.WeaknessPoints;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public class MobBuilder {
    private String name;
    private int level;
    private String mobid;
    private List<PotionEffectType> potioneffects = new ArrayList<>();
    private List<Spell> spells = new ArrayList<>();
    private List<ItemStack> loot= new ArrayList<>();
    private List<WeaknessPoints> weaknesspoints = new ArrayList<>();
    private ItemStack helmet,chestplate,leggings,boots,sword;
    private MobDefaultSettings mobDefaultSettings;


    public MobBuilder(MobDefaultSettings mobDefaultSettings){
        this.mobDefaultSettings = mobDefaultSettings;

    }

    public MobDefaultSettings getMobDefaultSettings() {
        return mobDefaultSettings;
    }

    public void setMobDefaultSettings(MobDefaultSettings mobDefaultSettings) {
        this.mobDefaultSettings = mobDefaultSettings;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public String getMobid() {
        return mobid;
    }

    public List<PotionEffectType> getPotioneffects() {
        return potioneffects;
    }

    public List<Spell> getSpells() {
        return spells;
    }

    public List<ItemStack> getLoot() {
        return loot;
    }

    public List<WeaknessPoints> getWeaknesspoints() {
        return weaknesspoints;
    }


    public ItemStack getHelmet() {
        return helmet;
    }

    public ItemStack getChestplate() {
        return chestplate;
    }

    public ItemStack getLeggings() {
        return leggings;
    }

    public ItemStack getBoots() {
        return boots;
    }

    public ItemStack getSword() {
        return sword;
    }

    public void setMobid(String mobid) {
        this.mobid = mobid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }


    public void setPotioneffects(List<PotionEffectType> potioneffects) {
        this.potioneffects = potioneffects;
    }

    public void setSpells(List<Spell> spells) {
        this.spells = spells;
    }

    public void setLoot(List<ItemStack> loot) {
        this.loot = loot;
    }

    public void setWeaknesspoints(List<WeaknessPoints> weaknesspoints) {
        this.weaknesspoints = weaknesspoints;
    }


    public void setHelmet(ItemStack helmet) {
        this.helmet = helmet;
    }

    public void setChestplate(ItemStack chestplate) {
        this.chestplate = chestplate;
    }

    public void setLeggings(ItemStack leggings) {
        this.leggings = leggings;
    }

    public void setBoots(ItemStack boots) {
        this.boots = boots;
    }

    public void setSword(ItemStack sword) {
        this.sword = sword;
    }

    public MobBuilder build(){
        return this;
    }
}
