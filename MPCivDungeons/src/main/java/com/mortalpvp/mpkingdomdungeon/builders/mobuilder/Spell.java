package com.mortalpvp.mpkingdomdungeon.builders.mobuilder;

import com.mortalpvp.mpkingdomdungeon.utils.particle.ParticleEffect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.potion.PotionEffect;

import java.util.LinkedList;

public class Spell {
    private String name;
    private LinkedList<Location> locs;
    private LinkedList<ParticleEffect> particleEffects;
    private double damage;
    private LinkedList<PotionEffect> potionEffects;
    private LinkedList<Sound> sounds;

    public Spell(String name, LinkedList<Location> locs, LinkedList<ParticleEffect> particleEffects, double damage, LinkedList<PotionEffect> potionEffects, LinkedList<Sound> sounds) {
        this.name = name;
        this.locs = locs;
        this.particleEffects = particleEffects;
        this.damage = damage;
        this.potionEffects = potionEffects;
        this.sounds = sounds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<Location> getLocs() {
        return locs;
    }

    public void setLocs(LinkedList<Location> locs) {
        this.locs = locs;
    }

    public LinkedList<ParticleEffect> getParticleEffects() {
        return particleEffects;
    }

    public void setParticleEffects(LinkedList<ParticleEffect> particleEffects) {
        this.particleEffects = particleEffects;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public LinkedList<PotionEffect> getPotionEffects() {
        return potionEffects;
    }

    public void setPotionEffects(LinkedList<PotionEffect> potionEffects) {
        this.potionEffects = potionEffects;
    }

    public LinkedList<Sound> getSounds() {
        return sounds;
    }

    public void setSounds(LinkedList<Sound> sounds) {
        this.sounds = sounds;
    }
}
