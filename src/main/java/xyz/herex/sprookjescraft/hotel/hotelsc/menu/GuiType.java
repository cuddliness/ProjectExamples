package xyz.herex.sprookjescraft.hotel.hotelsc.menu;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */

// Netjes, met enums werken is fijn en maakt je code overzichtelijk en haalt de kans op typfouten weg
// Juiste naming convention gebruikt
public enum  GuiType {
    NORMAL,PAGES
}
