package xyz.herex.sprookjescraft.hotel.hotelsc.command;

import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import xyz.herex.sprookjescraft.hotel.hotelsc.HotelSC;
import xyz.herex.sprookjescraft.hotel.hotelsc.filemanager.Config;
import xyz.herex.sprookjescraft.hotel.hotelsc.filemanager.Configs;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.Gui;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.menus.RoomMenu;
import xyz.herex.sprookjescraft.hotel.hotelsc.room.Room;
import xyz.herex.sprookjescraft.hotel.hotelsc.utils.C;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */
// Als je voor SC gaat ontwikkelen, kunnen de bovenstaande comments overal weg. Voegt geen waarde toe in onze code :)



public class Command implements CommandExecutor {

    // Deze onCommand methode wordt erg lang. Je zou voor elk argument weer een andere methode kunnen aanroepen, precies zoals je doet met sendHelp()
    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        if(args.length == 0){
            sendHelp(sender);
            return true;
        }
        String[] usedArgs = new String[]{"list", "rent", "create", "add"};
        if(Arrays.stream(usedArgs).noneMatch(s -> s.equalsIgnoreCase(args[0]))) {
            sender.sendMessage(C.K("&9[Hotel]&f Je gebruikt geen geldige argumenten!"));
            return true;
        }
        // IntelliJ heeft een shortcut voor het juist formatten en uitlijnen van je codeblocks (CTRL+SHIFT+ALT+L)
        // Of via Code -> Reformat Code in het menu bovenaan
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("list")) {
                    Gui g = new RoomMenu();
                    g.open((Player) sender);
                }
            } else if (args.length ==2){

                if(args[0].equalsIgnoreCase("create")){
                    if(!sender.hasPermission("hotel.rooms.create")){
                        sender.sendMessage(C.K("&9[Hotel] &cJe hebt geen permissie!"));
                        return true;
                    }
                    Room m = HotelSC.getInstance().getRoomManager().getRoombyName(args[1]);
                    if(args[1].equalsIgnoreCase("")){
                        sender.sendMessage(C.K("&9[Hotel]&f Je hebt geen kamer opgegeven"));
                        return true;
                    }
                    if(m == null){
                        sender.sendMessage(C.K("&9[Hotel] &7" + args[1] + " &fis niet beschikbaar of niet gevonden!"));
                        return true;

                    }else{
                        if(HotelSC.getInstance().getWorldEditPlugin().getSelection((Player) sender).getMaximumPoint() != null && HotelSC.getInstance().getWorldEditPlugin().getSelection((Player) sender).getMinimumPoint() != null){
                            Location loc1 = HotelSC.getInstance().getWorldEditPlugin().getSelection((Player) sender).getMaximumPoint();
                            Location loc2 = HotelSC.getInstance().getWorldEditPlugin().getSelection((Player) sender).getMinimumPoint();
                            Config conf = new Config(m.getRoomLabel(), new File(HotelSC.getInstance().getDataFolder() + "/rooms/"));
                            conf.getConfig().set("loc1", loc1);
                            conf.getConfig().set("loc2", loc2);
                            conf.saveConfig();
                            conf.reloadConfig();
                            sender.sendMessage(C.K("&9[Hotel] &7" + args[1] + " &fSelectie opgeslagen voor kamer: &7" + args[1]));
                            return true;
                        }else{
                            sender.sendMessage(C.K("&9[Hotel] &7" + args[1] + " &fWorldEdit selectie niet gevonden!"));
                            return true;
                        }
                    }

                }else if(args[0].equalsIgnoreCase("add")){
                    Room m = HotelSC.getInstance().getRoomManager().playerGetRoom((Player) sender);
                    if(m == null){
                        sender.sendMessage(C.K("&9[Hotel] &fJe hebt geen kamer!"));
                        return true;
                    }else {
                        OfflinePlayer player = Bukkit.getPlayer(args[1]);
                        if (player == null) {
                            sender.sendMessage(C.K("&9[Hotel] &fPersoon bestaat niet!"));
                            return true;
                        } else if (player.getUniqueId() == ((Player) sender).getUniqueId()) {
                            sender.sendMessage(C.K("&9[Hotel] &fJe kan jezelf niet toevoegen aan een kamer!"));

                        } else {
                            Config conf = new Config(m.getRoomLabel(), new File(HotelSC.getInstance().getDataFolder() + "/rooms/"));

                            List<String> list = conf.getConfig().getStringList("friends");
                            list.add(player.getUniqueId().toString());
                            conf.saveConfig();
                            m.getInvitedplayers().add(player.getUniqueId().toString());
                            sender.sendMessage(C.K("&9[Hotel] &fJe hebt: &9" + player.getName() + " Toegevoegd aan je hotel kamer!"));
                        }
                    }
                }

            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("rent")) {
                    if(!sender.hasPermission("hotel.rooms.rent")){
                        sender.sendMessage(C.K("&9[Hotel] &cJe hebt geen permissie!"));
                        return true;
                    }
                    if (Bukkit.getPlayer(args[1]) == null || !Bukkit.getPlayer(args[1]).isOnline()) {
                        sender.sendMessage(C.K("&9[Hotel]&f De speler is nu niet in het park!"));
                        return true;
                    }
                    Room m = HotelSC.getInstance().getRoomManager().getRoombyName(args[2]);
                    if (m == null) {
                        sender.sendMessage(C.K("&9[Hotel] &7" + args[2] + " &fis niet beschikbaar of niet gevonden!"));
                        return true;
                    } else if (m.getCornerLocations() == null) {
                        sender.sendMessage(C.K("&9[Hotel] &7" + args[2] + " &fHeeft nog geen locatie"));
                        return true;

                    } else {
                        Config conf = new Config(args[2], new File(HotelSC.getInstance().getDataFolder() + "/rooms/"));
                        conf.getConfig().set("houder", ((Player) sender).getUniqueId().toString());
                        conf.getConfig().set("rented", true);
                        conf.saveConfig();
                        conf.reloadConfig();
                        m.setRented(true);
                        m.setOwner(((Player) sender).getUniqueId().toString());
                        sender.sendMessage(C.K("&9[Hotel] &7" + args[2] + " is verhuurd aan: " + ((Player) sender).getName()));
                    }
                }
            }

            if(args.length > 3){
                sendHelp(sender);
                return true;
            }
        return false;
    }

    private void sendHelp(CommandSender sender){
        sender.sendMessage(C.K("&9> &f/room list - &9 laat alle hotel kamers zien!"));
        sender.sendMessage(C.K("&9> &f/room rent <player> <kamernaam> - &9 laat je een hotelkamer verhuren!"));
        sender.sendMessage(C.K("&9> &f/room create <KamerNaam> - &9Zet een locatie van een kamer"));
        sender.sendMessage(C.K("&9> &f/room add <player> - &9Voeg een player toe aan je kamer"));


    }
}
